(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
 			 ("melpa" . "https://melpa.org/packages/")
 			 ("ublt" . "https://elpa.ubolonton.org/packages/")
 			 ("org" . "https://orgmode.org/elpa/")))

 (package-initialize)
 (unless (package-installed-p 'use-package)
   (package-refresh-contents)
   (package-install 'use-package))

(require 'use-package)
 (setq use-package-compute-statistics t)
(setq use-package-always-ensure t)

(setq make-backup-files nil)

(use-package highlight-indent-guides
        :ensure t
        :hook
          (prog-mode . highlight-indent-guides-mode)
              :custom
         (highlight-indent-guides-method 'character)
         (highlight-indent-guides-responsive 'top))
         (setq-default indent-tabs-mode nil)

         (set-face-attribute 'default nil
                        :family "Iosevka Nerd Font"
                        :height 110)


        (use-package display-line-numbers
           :ensure nil
           :hook (prog-mode . display-line-numbers-mode))

(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)
(blink-cursor-mode -1)

(use-package doom-themes
 :ensure t
 :config
 (setq doom-themes-enable-bold t   
       doom-themes-enable-italic t) 
 (load-theme 'doom-tokyo-night t)

 (doom-themes-visual-bell-config)
 (doom-themes-neotree-config)
 (doom-themes-org-config))

(use-package all-the-icons)
(use-package minimap
  :ensure t
  :init
  (setq minimap-window-location 'right
        minimap-update-delay 0)
  :custom
  (custom-set-faces
   '(minimap-active-region-background ((t (:background "#1a1b26"))))
   '(minimap-inactive-region-background ((t (:background "#1f2335")))))
  :config
  (minimap-mode 1))

(use-package hl-line
 :config
 (global-hl-line-mode)
 (set-face-attribute 'hl-line nil :background "#1a1b26" :extend t))

(use-package dashboard
     :ensure t
     :init 
      (setq dashboard-banner-logo-title "Welcome to Thalene Emacs <3")
      (setq dashboard-center-content t)
      (setq dashboard-show-shortcuts nil)   
      (setq dashboard-items '((recents  . 5)
                             (bookmarks . 5)
                             (agenda . 5)
                             (registers . 5)))

   ;; All the icons  
   (setq dashboard-set-file-icons t)
   (setq dashboard-set-heading-icons t)
   ;; Dashboard 
   (setq dashboard-startup-banner "~/.emacs.d/images/dashboard.png")  
   :config 
   (dashboard-setup-startup-hook))

  (use-package neotree
    :ensure t
    :init
   (global-set-key [f8] 'neotree-toggle)
   (setq neo-theme (if (display-graphic-p) 'icons 'arrow))) 

(use-package centaur-tabs
  :ensure t
  :config
  (global-set-key (kbd "M-<left>")  'centaur-tabs-backward)
  (global-set-key (kbd "M-<right>") 'centaur-tabs-forward)
  (setq centaur-tabs-set-bar 'zigzag
        centaur-tabs-set-icons t
        centaur-tabs-gray-out-icons 'buffer
        centaur-tabs-height 30
        centaur-tabs-set-modified-marker t
        centaur-tabs-set-bar 'under
        x-underline-at-descent-line t
        centaur-tabs-modified-marker "*"
        centaur-tabs-close-button "●"  ; Utiliza el carácter Unicode para un círculo
        centaur-tabs-close-icon (all-the-icons-octicon "primitive-dot" :height 0.8 :v-adjust 0 :face 'centaur-tabs-unselected))
  (centaur-tabs-mode t))


  (use-package org-bullets
    :ensure t
    :init
    (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
    :config
    (setq org-bullets-bullet-list '("◉" "○" "●" "○" "●")))

  (use-package doom-modeline
   :ensure t
   :init (doom-modeline-mode 1)
    (setq doom-modeline-height 25))

;;Autocomplete () <> 
(show-paren-mode)
(electric-pair-mode)

;; Autocomplete Comands
(use-package which-key
  :ensure t
  :init
  (setq which-key-separator " ")
  (setq which-key-prefix-prefix "+")
  :config
  (which-key-mode))

(setq warning-minimum-level :emergency)

(unless (package-installed-p 'magit)
(package-refresh-contents)
(package-install 'magit))

(use-package elcord
   :ensure t
   :init
 (elcord-mode))

(use-package lsp-mode
  :ensure t
  :commands (lsp lsp-deferred)
  :hook
  ((rust-mode . lsp-deferred))
  :init
  (setq lsp-keymap-prefix "C-c l") ; Prefijo para los atajos de lsp-mode
  (setq lsp-rust-server 'rust-analyzer) ; Configura el servidor LSP para Rust
  (setq lsp-rust-analyzer-server-command '("rust-analyzer"))
;;  (setq lsp-enable-file-watchers nil) ; Desactiva los watchers por defecto
;;  (setq lsp-enable-on-type-formatting nil) ; Desactiva formateo al escribir
;;  (setq lsp-headerline-breadcrumb-enable nil) ; Desactiva breadcrumbs en el headerline
  (setq lsp-modeline-diagnostics-enable t) ; Muestra diagnósticos en el modeline
  (setq lsp-signature-auto-activate nil) ; Desactiva la activación automática de firmas
  (setq lsp-signature-render-documentation t) ; Muestra la documentación en las firmas
  (setq lsp-modeline-diagnostics-scope :workspace) ; Alcance de diagnósticos en el modeline
  :config
  (add-hook 'lsp-mode-hook 'lsp-enable-which-key-integration))

(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)

(use-package company
:ensure t
:hook (after-init . global-company-mode)
:config
(setq company-minimum-prefix-length 1
      company-idle-delay 0.0
      company-tooltip-limit 20))

(use-package rustic
  :ensure t
  :config
  (setq rustic-lsp-server 'rust-analyzer)
  (add-hook 'rustic-mode-hook 'setup-rustic-mode))

(defun setup-rustic-mode ()
"Setup rustic-mode."
(setq-local company-backends
            (append '((company-capf company-dabbrev-code))
                    company-backends)))
